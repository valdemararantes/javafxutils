package br.com.galgo.javafx

import javafx.application.Platform
import javafx.scene.control.Alert
import javafx.scene.control.ButtonType
import org.slf4j.LoggerFactory

/**
 * Created by valdemar.arantes on 07/11/2016.
 */

val log = LoggerFactory.getLogger("br.com.galgo.javafx.AlertUtilsKt")

fun showErrorAndContinue(msg: String, btnLabel: String): Boolean {
    val ret = arrayOf<Boolean?>(null);
    Platform.runLater {
        val alert = Alert(Alert.AlertType.ERROR, msg)
        val btnOk = ButtonType("OK")
        val btnCancelRequest = ButtonType(btnLabel)
        alert.buttonTypes.setAll(btnOk, btnCancelRequest)
        val result = alert.showAndWait()
        ret[0] = result.get() == btnOk
    }
    while (true) {
        if (ret[0] == null) {
            try {
                Thread.sleep(2000)
            } catch (ignored: InterruptedException) {
            }
        } else {
            return ret[0]!!
        }
    }
}

fun showInfo(txt: String) {
    showAlert(txt, Alert.AlertType.INFORMATION)
}

fun showError(txt: String) {
    showAlert(txt, Alert.AlertType.ERROR)
}

fun showAlert(txt: String, type: javafx.scene.control.Alert.AlertType) {
    Platform.runLater {
        val alert = javafx.scene.control.Alert(type, txt)
        alert.showAndWait().filter {
            response -> response == ButtonType.OK }.ifPresent { response -> log.info("Alert fechado")
        }
    }
}

